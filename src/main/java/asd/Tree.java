package asd;

public class Tree {

    Node root;

    public Tree(Node root) {
        this.root = root;
    }

    public Tree(String data) {
        this.root = new Node(data);
    }

    public boolean isEmpty() {
        return root == null;
    }

    public void add(String parent, String data) {
        Node node = getNode(root, parent);
        if (node != null) {
            node.childs.add(new Node(data));
        } else {
            System.out.println("Node pparent tidak ditemukan");
        }
    }

    public Node getNode(Node node, String data) {
        if (node.data == data) {
            return node;
        }
        for (Node currNode : node.childs) {
            Node returnNode = getNode(currNode, data);
            if (returnNode != null) {
                return returnNode;
            }
        }
        return null;
    }

    public void access() {
        System.out.print(root.data);
        access(root, " ");
    }

    public void access(Node node, String space) {
        System.out.println("");
        for (Node currNode : node.childs) {
            System.out.print(space + "->" + currNode.data);
            access(currNode, space + " ");
        }
        return;
    }
}
